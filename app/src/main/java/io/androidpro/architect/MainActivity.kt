@file:OptIn(ExperimentalMaterial3Api::class)

package io.androidpro.architect

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import io.androidpro.architect.ui.theme.ArchitectAppsTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ArchitectAppsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    OrderListScreen()
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OrderListScreen() {
    Scaffold(
        topBar = {
            TopAppBar(title = { Text("Marketplace") })
        }
    ) { padding ->
//        OrderList(padding)
        OrderDetails("0", padding)
    }
}


@Composable
fun OrderList(padding: PaddingValues) {

    var data by remember { mutableStateOf<List<Order>>(emptyList()) }

    LaunchedEffect(Unit) {
        val orders = withContext(Dispatchers.IO) {
            FakeRepository.getAll()
        }
        data = orders
    }

    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(padding),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        items(data) { order ->
            OrderItemCard(order) {
                //OrderList: clicked $order
            }
        }
    }
}

@Composable
fun OrderItemCard(order: Order, clicked: (Order) -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable { clicked(order) }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Text(
                text = "Order ID: ${order.orderId}",
                fontWeight = FontWeight.Bold,
                fontSize = 18.sp
            )
            Text(text = "Customer: ${order.customerName}")
            Text(text = "Restaurant: ${order.restaurantName}")
            Text(text = "Status: ${order.orderStatus}")
        }
    }
}


@Composable
fun OrderDetails(orderId: String, padding: PaddingValues) {

    var order by remember { mutableStateOf<Order?>(null) }

    LaunchedEffect(Unit) {
        val orderById = withContext(Dispatchers.IO) {
            FakeRepository.getById(orderId)
        }
        order = orderById
    }

    if (order != null) {
        // Display the order details
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
                .padding(padding)
        ) {

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                Text(
                    text = "Order ID: ${order!!.orderId}",
                    style = MaterialTheme.typography.headlineSmall
                )
                Text(text = "Customer: ${order!!.customerName}")
                Text(text = "Customer Address: ${order!!.customerAddress}")
                Text(text = "Customer Contact: ${order!!.customerContact}")
                Text(text = "Restaurant: ${order!!.restaurantName}")
                Text(text = "Restaurant Address: ${order!!.restaurantAddress}")
                Text(text = "Special Instructions: ${order!!.specialInstructions ?: "None"}")
                Text(text = "Order Status: ${order!!.orderStatus}")

                // Displaying Order Items
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 16.dp),
                    verticalArrangement = Arrangement.spacedBy(4.dp)
                ) {
                    Text(text = "Items:", style = MaterialTheme.typography.headlineSmall)
                    order!!.orderItems.forEach { item ->
                        Text(text = "${item.quantity}x ${item.itemName} - \$${item.price}")
                    }
                }
            }
        }
    }


}
